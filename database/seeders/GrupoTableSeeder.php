<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class GrupoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $grupos = [
            ['id' => 1, 'nome' => 'Cliente'],
            ['id' => 2, 'nome' => 'Vendedor']
        ];

        foreach ($grupos as $grupo) {
            if(\DB::table('grupo')->where('id', $grupo['id'])->doesntExist()) {
                \DB::table('grupo')->updateOrInsert([
                    'id'   => $grupo['id'],
                    'nome' => $grupo['nome']
                ]);
            }
        }
    }
}
