<?php

namespace App\Repositories;

use App\Models\Usuario;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Repositories\Contracts\DbUsuarioRepositoryInterface;

class DbUsuarioRepository implements DbUsuarioRepositoryInterface
{
    private $model;

    public function __construct (Usuario $model) {
        $this->model = $model;
    }

    public function cadastrarUsuario ($request) {

        try {
            $this->model->create([
                'nome'     => $request->nome,
                'email'    => $request->email,
                'senha'    => Hash::make($request->senha),
                'grupo_id' => $request->grupo_id
            ]);

            return 'STATUS CODE: '.Response::HTTP_OK;
        } catch (\Throwable $th) {
            return [
                'erro ' => 'Erro ao cadastrar usuário!'
            ];
        }
        
    }
}
