<?php

namespace App\Repositories\Contracts;

interface DbUsuarioRepositoryInterface
{
    public function cadastrarUsuario($request);
}
