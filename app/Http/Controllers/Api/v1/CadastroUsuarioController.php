<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Contracts\DbUsuarioRepositoryInterface;

class CadastroUsuarioController extends Controller
{

    private $usuarioRepository;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(DbUsuarioRepositoryInterface $usuarioRepository)
    {
        $this->usuarioRepository = $usuarioRepository;
    }

    public function cadastrarUsuario (Request $request) {
        return response()->json($this->usuarioRepository->cadastrarUsuario($request));
    }
}
