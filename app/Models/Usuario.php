<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nome',
        'email',
        'senha',
        'grupo_id'
    ];

    protected $table = 'usuario';

    public function helloModel () {
        return 'Model funcionando!';
    }

}
